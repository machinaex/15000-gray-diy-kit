/* Telefon
* die Impulse eines Impulswahltelefones werden über den analogRead
* an pin 5 ausgelesen und interpretiert, so dass der Serial die
* gedrückten Tasten ausgibt.
********************************/

#define telLED 11 // Telefon Test LED

long time = 100;

//Telefon Variablen
boolean stop1 = false; // true verhindert die mehrmalige ausführung des Impulszählers
boolean stop2 = false; // false erlaubt Serial.print bei auflegen

int takt = 8; // Zeitintervall länger als die einzelnen impulse

int anzeit = 0; // über millis() definierte zeiten
int anzeitSpanne = 0;
int auszeit = 0;
int auszeitSpanne = 0;

int zaehler = 0; // Counter für Impulssignale
int nummer = 0;
int taste = 10; // wird nur im Wertebereich 0-9 gesendet

void setup()
{
  Serial.begin(115200);  //Initialisierung des USB Ports
  pinMode(telLED, OUTPUT); //Telefon Test LED
}

void loop() {

  int telefon = analogRead(5);
  auszeitSpanne = millis() - auszeit;
  anzeitSpanne = millis() - anzeit;
  
  // Wenn Hörer abgenommen
  if (telefon > 1 && anzeitSpanne > takt)
  {
    digitalWrite(telLED, HIGH);
    auszeit = millis(); //die Auszeitspanne wieder auf 0 setzen
    if (zaehler > 30) // wenn tastenimpuls (etwa 40 mal 0/1) nummer hochzählen
    {
      nummer++;
    }
    zaehler = 0; // Zähler zurücksetzen
    stop2 = false; // freischalten des Unterbrechungs Serial.print
  }
  // Um zwischen den einzeltasten die tastennummer auf null zu setzen
  if (telefon > 1 && anzeitSpanne > 100)
  {
    if (nummer == 10) 
    {
      taste = 0;
    } else if (nummer > 0) {
      taste = nummer;
    }
    if (taste < 10) 
    {
      Serial.print(taste);
    }
    nummer = 0;
    taste = 10;
  }
  // Wenn Impulssignal 1
  if (telefon > 1 && anzeitSpanne <= takt)
  {
    stop1 = false;
    auszeit = millis();
    digitalWrite(telLED, LOW);
  }
  // Wenn Impulssignal 0
  if (telefon <= 5 && auszeitSpanne <= takt && stop1 == false) 
  {
    zaehler++;
    //Serial.print(zaehler);
    anzeit = millis(); //Anzeitspanne wird wieder auf 0 gesetzt
    stop1 = true;
    digitalWrite(telLED, LOW);
  }
  
  // Wenn Hörer aufgelegt
  if (telefon <= 5 && auszeitSpanne > takt)
  {
    digitalWrite(telLED, LOW);
    zaehler = 0;
    taste = 10;
    nummer = 0;
    anzeit = millis();
    if (stop2 == false)
    {
      Serial.print("x");
    }
    stop2 = true;
  }
  
}
