/*
 * Dieser Sketch ist Teil der Bastelanleitung fuer das Real Live Game Adventures
 * 15000 Gray von machina eX.
 * 
 * Spiele ihn auf den Arduino oder Wemos fuer die Uhr
 * auf um sie in das Game zu integrieren.
 * 
 * Mehr Informationen und Anleitungen findest du hier:
 * https://www.machinaex.de/15000-gray-diy-kit/
 *
 * Ver.0.2 29.01.2018
 */


// Variablen für die Magnetkontakter Werte 
bool reedA = false;
bool reedAstate = false;

bool reedB = false;
bool reedBstate = false;

int reedApin = 2;
int reedBpin = 3;

/*
* setup() wird bei Start einmal ausgefuehrt
*/
void setup() {
  // Serial Verindung herstellen
  Serial.begin(115200);

  // Digital pins auf dem Arduino initialisieren
  pinMode(reedApin, INPUT);
  pinMode(reedBpin, INPUT);

  Serial.println("Die Uhr laeuft.");
}

/*
* Wird endlos wiederholt nachdem setup() durchgefuehrt wurde
*/
void loop() {

  // In Variable uebertragen ob eine Spannung an pin A oder B anliegt
  reedA = digitalRead(reedApin);
  reedB = digitalRead(reedBpin);
  
  // Kontakt A auf Aenderung ueberpruefen
  if(reedA != reedAstate)
  {
    Serial.print("Reedkontakt A: ");
    Serial.println(reedA);
    
    // Wenn beide Kontakte geschlossen sind, Nachricht ueber Netzwerk versenden
    if ( reedA == true && reedB == true )
    {
      Serial.println("Die Zeiger stehen auf 8 Uhr");
    } else {
      Serial.println("Die Zeiger stehen NICHT auf 8 Uhr");
    }
    
    reedAstate = reedA;
  }

  // Kontakt B auf Aenderung ueberpruefen
  if(reedB != reedBstate)
  {
    Serial.print("Reedkontakt B: ");
    Serial.println(reedB);

    // Wenn beide Kontakte geschlossen sind, Nachricht ueber Netzwerk versenden
    if ( reedA == true && reedB == true )
    {
      Serial.println("Die Zeiger stehen auf 8 Uhr");
    } else {
      Serial.println("Die Zeiger stehen NICHT auf 8 Uhr");
    }
    
    reedBstate = reedB;
  }
}
