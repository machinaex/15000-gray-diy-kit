Numpad
-------

Das Numpad, dass du mit dieser Anleitung zusammenbauen kannst wird in 15000 Gray zum Sicherheitszugang für Rechner des Labors.

Wird der richtige dreistellige Code eingegeben, löst das Numpad den nächsten Cue (NUMPAD_SOLVED) aus.

Gut geeignet für einen verregneten Tag mit leichten Bastel und Lötaufgaben.
Wenn du Einsteiger in die Welt der DIY Elektronik bist, empfehlen wir dir, zunächst die [Uhr](../Uhr) zu bauen oder einen Blick auf die ["getting started" Seite der Arduino Homepage](https://www.arduino.cc/en/Guide/HomePage) zu werfen.

* [Werkzeug](#markdown-header-werkzeug)
* [Bauteile](#markdown-header-bauteile)
* [Zusammenbauen](#markdown-header-zusammenbauen)
* [Code aufspielen](#markdown-code-aufspielen)
* [Testen](#markdown-code-testen)
* [Tweaken](#markdown-code-tweaken)

![15000GrayNumpad](img/numpad_eingabe.gif "Spoiler Alert!")

###Werkzeug

* Lötwerkzeug
* Abisolierzange
* Cutter / Teppichmesser
* Akkuschrauber mit 5mm Holzbohrer

###Bauteile

![Bauteile für das Numpad Abbildung](img/bauteile.png "Bauteile für das Numpad")

* [Wemos Mikrocontroller](https://www.aliexpress.com/store/product/D1-mini-Mini-NodeMcu-4M-bytes-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/1331105_32529101036.html)
* [Tastenfeld von Conrad](https://www.conrad.de/de/drucktastenfeld-tastenfeld-matrix-3-x-4-1-st-709840.html) oder [Exp-Tech](https://www.exp-tech.de/zubehoer/tasterschalter/4377/keypad-12-button-com-08653)
* [Gehäuse](https://www.conrad.de/de/euro-gehaeuse-150-x-80-x-50-abs-grau-weroplast-1010-1-st-520586.html)
* verschiedenfarbige Jumperkabel
* [Streifenraster Platine](https://www.conrad.de/de/platine-epoxyd-l-x-b-160-mm-x-100-mm-35-m-rastermass-254-mm-wr-rademacher-wr-typ-710-5-inhalt-1-st-529519.html)
* [Grüne und rote LED](https://www.conrad.de/de/led-bedrahtet-gruen-rund-5-mm-175-mcd-50-20-ma-tru-components-1577389.html?fromReco=1&recoReferrerProduct=1577388&recoType=similar)
* 2 Platinen Buchsenleisten im Rastermaß 2,54 mit jeweils 8 Buchsen
* 2 Stiftleisten im Rastermaß 2,54 mit 10 und 12 Stiften


###Zusammenbauen

Schneide dir eine Streifenrasterplatine auf ca. 60mmx60mm zurecht.

Verlöte die 8er Buchsenleisten auf der Platine quer zu den Leiterbahnen, so dass du das Wemos aufstecken kannst und eine Verbindung auf der Platine zu jedem Pin des Wemos besteht.

Verlöte die Widerstände auf der Platine so, dass einer mit dem D7 und einer mit dem D8 Pin verbunden ist wenn du das Wemos aufsteckst. Verlöte das andere Ende jeweils mit einer freien Leiterbahn.

Benutze einen Cutter oder eine schmale Feile um die Leiterbahnen der Platine in der Mitte der Längsseite zu unterbrechen

Verlöte die 10er Buchsenleiste auf der Platine gegnüber der Widerstände so dass die Buchsen mit den Pins des Wemos verbunden und zwei Pins überragen. Verbinde nun den mit dem GND Pin verbundenen Leiter mit einem der freien Pins.
Die 12er Pinleiste wird auf der anderen Seite Verlötet. Der äußerste überragende Pin muss mit dem äußeren der Widerstände verbunden sein.

![platine_vorne](img/platine_vorderseite.png "Vorderseite Platine")
![platine_rueck](img/platine_rueckseite.png "Rückseite der Platine")

Schneide mit ca. 5cm Abstand das Buchsenende eines blauen, eines grünen und von zwei schwarzen Jumper Kabeln ab und entferne ca. 5mm der Isolierung am abgeschnittenen Ende.  
Verlöte jeweils das schwarze Kabel mit der Kathode, also der abgeflachten Seite der LED und das rote Kabel mit der Anode.

![leds1](img/led_verkabeln1.png "Der GND kommt an die ...")
![leds2](img/led_verkabeln2.png "... abgeflachte Seite")
![leds3](img/leds_verkabelt.png "Plus Schrumpfschlauch für die Optik")

Messe das Tastenfeld aus und schneide mit einem scharfen Cutter ein passendes Rechteck in das Gehäuse. Wenn du ein Multitool, etwa einen Dremel, zur Hand hast kannst du hier etwas Zeit sparen.  
Ggf. kannst du noch Löcher hinzufügen um das Tastenfeld mit dem Gehäuse zu verschrauben.

Mache mit einem Akkubohrer zwei 5mm Löcher für die LEDs mittig ober oder unterhalb des Tastenfeldes ins Gehäuse.

![gehaeuse_vorbereiten](img/gehaeuse_vorbereitung.png "Maß nehmen")
![gehaeuse](img/gehaeuse.png "Plastik schnitzen ist schön, macht aber viel Arbeit")

Setze das Tastenfeld und Die LEDs ins Gehäuse ein. Verklebe die LEDs mit etwas Sekundenkleber und verklebe oder verschraube das Tastenfeld im Gehäuse.

![bauteile_einsetzen](img/bauteile_einsetzen.png "Nicht im Bild: Sekundenkleber")

Damit hinterher nichts mehr wackelt kannst du deine Platine noch mit dem Gehäusedeckel verschrauben.

![platine_verschrauben](img/platine_verschraubt.png "Beliebte Alternative wäre doppelseitiges Klebeband")

Verbinde nun die Pins des Tastenfelds und der LED über die Platinenstecker mit den Stiftleisten auf deiner Platine wie in der Grafik abgebildet. Die Pins des Tastenfeldes werden von links nach rechts (von hinten auf das Tastenfeld geblickt) mit den Pins D0 - D6 des Wemos verunden.

![schaltplan](img/numpad_wemos_schaltplan.png "Wird schon passen")

Stecke die LED jumper auf die mit den Widerständen verbundenen Pins und mit dem schwarzen Jumper auf die beiden Pins die mit GND verbunden sind.

![verkabeln](img/verkabeln.png "Wird schon passen")
![fertig](img/fertig.png "15000 Grey")


###Code aufspielen

Wie du den WEMOS mit der Datei `numpad_wemos.ino` bespielst kannst du [hier](../README.md) im Abschnitt *Code aufspielen* nachlesen.

Öffne die Serial Console und überprüfe ob du jeweils die letzten drei eingegebenen Tasten angezeigt bekommst.

Den richtigen Code findest du *trommelwirbel* im Code :)

![console](img/numpad_console.png "Setze die Baud Rate auf 115200")

###Testen

Setze den Aufbau zunächst mit einem Arduino UNO auf dem Breadboard um.

![testaufbau](img/test_aufbau.png "Numpad Testaufbau")
![testaufbau_totale](img/test_aufbau2.png "Numpad Testaufbau")

Die Verkabelung ist aufrgund des unterschiedlichen Platinendesigns etwas anders als beim Wemos.

![schaltplan_arduino](img/numpad_arduino_schaltplan.png "Auf dem Breadboard")

Schließe den Arduino an deinen Rechner an und öffne die Datei `numpad_arduino.ino`.

Spiele den Sketch auf und überprüfe über den Arduino Serial Monitor ob du die Tasten angezeigt bekommst und ob sich die LEDs richtig verhalten.

###Tweaken

Du kannst sehr einfach die Zahl die zur Lösung führt verändern, indem du dir im Code die folgende Zeile vornimmst:

`if(strcmp(letzteEingabe, "169") == 0)`

ändere die 169, die in 15000 Gray gesucht wird in eine beliebige dreistellige Zahl um.