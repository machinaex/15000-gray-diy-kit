/*
 * Dieser Sketch ist Teil der Bastelanleitung fuer das Real Live Game Adventures
 * 15000 Gray von machina eX.
 * 
 * Spiele ihn auf dein Arduino auf um das Numpad zu testen.
 * 
 * Um es in die zentrale Schnittstellenverwaltung von 15000 Gray einzubinden benutze die WEMOS version dieses sketches.
 * 
 * Mehr Informationen und Anleitungen findest du hier:
 * https://www.machinaex.de/15000-gray-diy-kit/
 *
 * Ver.0.1 30.01.2018
 * 
 * Um das Numpad ohne grossen Aufwand nutzen zu koennen binden wir die 
 * keypad library for arduino von Mark Stanley in unseren Code ein:
 * https://playground.arduino.cc/Code/Keypad
 * Installiere die Library über Sketch -> include library -> manage libraries
 * tippe "keypad" in das Suchfeld ein und installiere die Keypad Library von Mark Stanley
 */

// Die Keypad Library einbinden
#include <Keypad.h>

// Variablen fuer Keypad
const byte ROWS = 4; // Vier Reihen
const byte COLS = 3; // Drei Zeilen
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'#','0','*'}
};
byte rowPins[ROWS] = {5, 6, 7, 8}; // mit den Reihen Pins verbunden
byte colPins[COLS] = {2, 3, 4}; // mit den Zeilen Pins verbunden

// Keypad Objekt aus Keypad Library erstellen
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// Digital Pin Variablen fuer die LEDs
int gruen = 9;
int rot = 10;

// String zum speichern der letzten drei Eingaben
String letzteEingabe = "xxx";

void setup(){
  Serial.begin(115200);
  Serial.println("15000 Gray Numpad");

  pinMode(gruen, OUTPUT);
  pinMode(rot, OUTPUT);

  // Die LEDs eine Sekunde aufleuchten lassen
  digitalWrite(gruen, HIGH);
  digitalWrite(rot, HIGH);

  delay(1000);

  digitalWrite(gruen, LOW);
  digitalWrite(rot, LOW);
}

void loop(){
  char key = keypad.getKey();

  if (key != NO_KEY){
    // Die chars (Buchstaben) im letzteEingabe String um eine Stelle nach links verschieben
    letzteEingabe[0] = letzteEingabe[1];
    letzteEingabe[1] = letzteEingabe[2];
    letzteEingabe[2] = key;

    Serial.println(letzteEingabe);

    // Die letzte Eingabe mit dem Loesungscode vergleichen
    if(letzteEingabe == "169")
    {
      Serial.println("Numpad geloest");
      digitalWrite(gruen, HIGH);
      digitalWrite(rot, LOW);
    } else {
      digitalWrite(rot, HIGH);
      digitalWrite(gruen, LOW);
    }
  }
}
