// ============== Board Class ================
// ===========================================
public class Board{
  int score = 0;

  // constructor
  Board(){
  
  }

  void clearFullRows(){
    int numFullRows = 0;
    for (int i=0; i<numRows; i++){
      if(myBoardBlock[0][i].isTaken() == true && myBoardBlock[1][i].isTaken() == true && myBoardBlock[2][i].isTaken() == true 
        && myBoardBlock[3][i].isTaken() == true && myBoardBlock[4][i].isTaken() == true && myBoardBlock[5][i].isTaken() == true 
        && myBoardBlock[6][i].isTaken() == true && myBoardBlock[7][i].isTaken() == true && myBoardBlock[8][i].isTaken() == true 
        && myBoardBlock[9][i].isTaken() == true)  {
        numFullRows ++;
        //println("full" + numFullRows);
        for (int j=0; j<numCol; j++){
          myBoardBlock[j][i].take();

          for (int k = i; k>0; k--){
            myBoardBlock[j][k].taken = myBoardBlock[j][k-1].taken;
            myBoardBlock[j][k].col = myBoardBlock[j][k-1].col;
          }
        }
      }
    }
    if (numFullRows == 4){
      score += 40;      // Tetris!!
    }
    else if(numFullRows > 0){
      score = score + (numFullRows * 10);
      numRowsCleared += numFullRows;
      OscMessage mytetris = new OscMessage("/tetris");
      mytetris.add("fullrow");
      oscP5.send(mytetris, myRemoteLocation);
    }
    numFullRows = 0;
    if(score == 100){
      OscMessage mytetris = new OscMessage("/tetris");
      mytetris.add("tetriswin");
      oscP5.send(mytetris, myRemoteLocation);
    }
  }


  void renderBoard(){    
    for (int i=0; i<10; i++){
      for (int j=0; j<15; j++){
        myBoardBlock[i][j].render();
      }
    } 
    noFill();
    strokeWeight(1);
    rect(offset,offset, brickSize*numCol, brickSize*numRows);
  }


  void gameOver(){
    gameIsOver = true;
   // println("Game Over");
  }


  void newGame(){
    score = 0;
    gameIsOver = false;
    for (int i=0; i<numCol; i++){
      for (int j=0; j<numRows; j++){
        myBoardBlock[i][j].taken = false;
        myBoardBlock[i][j].col = color(51,51,51);
      }
    }
    myPiece = new fallingPiece((int)random(6.9999));
    //println("New Game");
  }

  void displayScore(){
    textFont(Hira,25);
    strokeWeight(2);
    stroke (0);
    fill(120,240,40);
    text("Firewall:", width-300,75);
    text("UPLOAD", width-630,75);
    rect(width-300,height-120-score*2,250,score*2);
    fill(120,240,40);
    text(score + "%", width-300,110);
  }


  void displayGameOver(){
    if (gameIsOver == true){
      int bX = 70;
      int bY = 125;
      int bWidth = 140;
      int bHeight = 32;

      fill(0);
      rect(offset,offset, brickSize*numCol, brickSize*numRows);
      textFont(Hira,25);
      strokeWeight(1);
      fill(120,240,40);
      text("Upload vollständig!", 70,300); //Crypta hat es geschafft
      OscMessage mytetris = new OscMessage("/tetris");
      mytetris.add("tetrislose");
      oscP5.send(mytetris, myRemoteLocation);
      //state = 4; 

      //noStroke();
      //fill(20,30,200);
      //rect(bX,bY, bWidth,bHeight);
      //fill(120,240,40);
      //text("New Game", 75,150);
      //stroke(100);

      // Click for new game
      //if(mouseX > bX && mouseX < bX + bWidth && 
        //mouseY > bY && mouseX < bY + bHeight){
        //if(mousePressed){
          //myBoard.newGame();
        //}
      //}
    }
  }


  // ============ Board Block sub class ===============
  public class BoardBlock extends Board{
    color col;
    int x;
    int y;
    boolean taken;

    // Constructor
    BoardBlock(int x, int y){
      col = color(51,51,51);
      this.x = offset+ x*25;
      this.y = offset+ y*25;
      this.taken = false;
    }

    // Accessor 
    
    boolean isTaken(){
     return taken; 
    }
    
    // Setter
    
    void take(){
      this.taken = true;
    }
    
    
    void render(){
      fill(col);
      if(taken){
        strokeWeight(1);
      }
      else{
        strokeWeight(1);
      }
      rect(x,y, brickSize,brickSize);
    }
  }
  
}
