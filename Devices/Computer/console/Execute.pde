//import java.awt.geom.*;

public class Execute
{int maxDepth;
  Execute()
  {
    maxDepth = 4;
    numDepth = 0;
    trackDepth = new int[maxDepth];
    trackDepth[0] = 0; // is C
  }
  void twoWords(int cmdNo, String secondArgument)
  {
    switch(cmdNo)
    {
      case 0://cd
        changeDirectory(secondArgument);
        break;
      case 1://run
        println("second argument is " + secondArgument);
        run(secondArgument);
        break;
      case 2://copy
        println("copy second argument is " + secondArgument);
        kopieren(secondArgument);
        break;  
    }
  }
  void oneWord(int cmdNo)
  {
    switch(cmdNo)
    {
      case 3://dir
        println("direcetory is now " + curDir.getString("path"));
        readDir(curDir);
        break;
      case 4://help
        help();
        break;
      case 5://cd..
        upOneLevel(curDir);
        break;
      case 6://A:
        changeDrive(2);
        break;
      case 7:// B:
        changeDrive(3);
        break;
      case 8:// D:
        changeDrive(1);
        break;   
      case 9:// C:
        changeDrive(0);
        break;    
    }
  }
  /* 
   * - function read out files and folders in current xml directory
  */
  
  void readDir(XMLElement tempDir)
  {
    int countCur = tempDir.getChildCount();
    String[] dirString = new String[countCur];
    for (int i = 0; i < countCur; i++)
    {
      XMLElement childDir;
      childDir = tempDir.getChild(i);
      String subFile = childDir.getString("path");
      dirString[i] = subFile;
      if (childDir.getString("ext") != null)
      {
        String extension = childDir.getString("ext");
        dirString[i] = dirString[i] + extension;
      }
    }
    textEngine.addText(dirString);
  }
  
  /*
   * - function sets new directory to parent of current directory
  */
  
  void upOneLevel(XMLElement curUpDir)
  {
    XMLElement changeDir;
    int deepness = curUpDir.getIntAttribute("depth");
    if (deepness > 1)
    {
      //trackDepth[numDepth] = 5;// custom null
      numDepth = numDepth - 1;
      String topDir = curDir.getParent().getString("path");
      //println(topDir);
      if (topDir.equals(xml.getChild(driveState).getString("path")))
      {
        println(curDir.getString("path"));
        fullDirectory = fullDirectory.substring(0,fullDirectory.length() - curDir.getString("path").length() - 1)+ ">";
        changeDir = curDir.getParent();
        curDir = changeDir;
      } else {
        int numSlash = (deepness * - 1)+1;
        fullDirectory = fullDirectory.substring(0,fullDirectory.length() - curDir.getString("path").length()+ numSlash)+ ">";
        changeDir = curDir.getParent();
        curDir = changeDir;
      }
    }
  }
  void changeDrive(int drive)
  {
    driveState = drive;
    curDir = xml.getChild(drive);
    trackDepth[0] = drive;
    numDepth = 1;
    fullDirectory = curDir.getString("path") + "\\>";
  }
  void help()
  {
    textEngine.addText(loadStrings("help.txt"));
  }
  
  void kopieren(String fileName) 
  {
     int countFiles = curDir.getChildCount();
     boolean wrongFile = true;
    //boolean fileError = false;
     for(int i = 0; i < countFiles; i++)
    {
      XMLElement file = curDir.getChild(i);
      String pathName = file.getString("path").toLowerCase();
      String ext ="";
      if (file.getString("ext") != null) 
      { 
        ext = file.getString("ext").toLowerCase();
      }
      println("path name is: " + pathName);
      println("filename is: " + fileName);
      println("extension is: " + ext);
      if(fileName.equals(pathName + ext) || fileName.equals(pathName))// bug? why not -> && file.getString("ext") != null);
      {
        if (file.getString("ext") != null)
        { 
          if(pathName.equals("paula"))
          {
            textEngine.addText(loadStrings("copy.txt"));
            textEngine.addString("copy paula.xls to disc");            
            OscMessage trigger = new OscMessage("/trigger");
            trigger.add("lose");
            oscP5.send(trigger, myRemoteLocation); 
            wrongFile = false;}
          if(pathName.equals("ronny"))
          {
            textEngine.addText(loadStrings("copy.txt"));
            textEngine.addString("copy ronny.xls to disc");
            OscMessage trigger = new OscMessage("/trigger");
            trigger.add("win");
            oscP5.send(trigger, myRemoteLocation); 
            wrongFile = false;} 
         }   
        if (wrongFile == true)// && fileError == false)
        {
           //fileError = true;
           textEngine.addString("<error 663: can't copy file permission required>");
        }
  }
  }
  }
  
  void run(String fileName)
  {
    int countFiles = curDir.getChildCount();
    boolean wrongFile = true;
    //boolean fileError = false;
    for(int i = 0; i < countFiles; i++)
    {
      XMLElement file = curDir.getChild(i);
      String pathName = file.getString("path").toLowerCase();
      String ext ="";
      if (file.getString("ext") != null) 
      { 
        ext = file.getString("ext").toLowerCase();
      }
      println("path name is: " + pathName);
      println("filename is: " + fileName);
      println("extension is: " + ext);
      if(fileName.equals(pathName + ext) || fileName.equals(pathName))// bug? why not -> && file.getString("ext") != null);
      {
        if (file.getString("ext") != null)
        { 
          if(pathName.equals("machina"))
          {
            textEngine.addText(loadStrings("machina.txt"));
            textEngine.addString("     is already runnig");
            wrongFile = false;} 
          if(pathName.equals("start"))
          {
            textEngine.addText(loadStrings("danke.txt"));
            wrongFile = false;}
          if(pathName.equals("computer"))
          {
            textEngine.addText(loadStrings("system.txt"));
            wrongFile = false;}
          if(pathName.equals("smile"))
          {
            textEngine.addText(loadStrings("smile.txt"));
            wrongFile = false;}
           if(pathName.equals("joke"))
          {
            textEngine.addText(loadStrings("dos.txt"));
            wrongFile = false;}  
           if(pathName.equals("helgabild"))
          {
            textEngine.addText(loadStrings("helga.txt"));
            wrongFile = false;}
           if(pathName.equals("logo"))
          {
            textEngine.addText(loadStrings("logomci.txt"));
            wrongFile = false;}
           if(pathName.equals("windas"))
          {
            textEngine.addText(loadStrings("windows.txt"));
            wrongFile = false;}    
           if(pathName.equals("feuchtwangen"))
          {
            textEngine.addText(loadStrings("Feuchtwangen.txt"));
            wrongFile = false;}  
           if(pathName.equals("gedicht"))
          {
            textEngine.addText(loadStrings("Gedicht.txt"));
            wrongFile = false;}
           if(pathName.equals("potentersteifer"))
          {
            textEngine.addText(loadStrings("PotenterSteifer.txt"));
            wrongFile = false;}    
           if(pathName.equals("hans"))
          {
            textEngine.addText(loadStrings("HansMail.txt"));
            wrongFile = false;}    
           if(pathName.equals("helga"))
          {
            textEngine.addText(loadStrings("HelgaMail.txt"));
            wrongFile = false;}
          if(pathName.equals("volker"))
          {
            textEngine.addText(loadStrings("VolkerMail.txt"));
            wrongFile = false;}   
          if(pathName.equals("monika"))
          {
            textEngine.addText(loadStrings("Monika.txt"));
            wrongFile = false;}  
          if(pathName.equals("warnung"))
          {
            textEngine.addText(loadStrings("WarnungMail.txt"));
            wrongFile = false;} 
          if(pathName.equals("news"))
          {
            textEngine.addText(loadStrings("News.txt"));
            wrongFile = false;}     
          if(pathName.equals("tag300"))
          {
            textEngine.addText(loadStrings("Tag300.txt"));
            wrongFile = false;}
          if(pathName.equals("tag316"))
          {
            textEngine.addText(loadStrings("Tag316.txt"));
            wrongFile = false;}
          if(pathName.equals("tag325"))
          {
            textEngine.addText(loadStrings("Tag325.txt"));
            wrongFile = false;}
          if(pathName.equals("tag340"))
          {
            textEngine.addText(loadStrings("Tag340.txt"));
            wrongFile = false;}
          if(pathName.equals("testreihen"))
          {
            textEngine.addText(loadStrings("test.txt"));
            wrongFile = false;}
          if(pathName.equals("versuchsnotizen"))
          {
            textEngine.addText(loadStrings("versuch.txt"));
            wrongFile = false;}
          if(pathName.equals("messungen"))
          {
            textEngine.addText(loadStrings("messung.txt"));
            wrongFile = false;}   
          if(pathName.equals("antinarc"))
          {
            textEngine.addText(loadStrings("Rezept.txt"));
            wrongFile = false;}
          if(pathName.equals("narcolep"))
          {
            textEngine.addText(loadStrings("Rezept2.txt"));
            wrongFile = false;} 
          if(pathName.equals("accid"))
          {
            textEngine.addText(loadStrings("Rezept3.txt"));
            wrongFile = false;} 
          if(pathName.equals("maus"))
          {
            textEngine.addText(loadStrings("maus.txt"));
            wrongFile = false;} 
          if(pathName.equals("bildchen"))
          {
            textEngine.addText(loadStrings("bildchen.txt"));
            wrongFile = false;}   
          if(pathName.equals("portrait"))
          {
            textEngine.addText(loadStrings("men.txt"));
            wrongFile = false;}  
          { if(pathName.equals("paula"))
          {
            textEngine.addText(loadStrings("paula.txt"));
           OscMessage trigger = new OscMessage("/trigger");
            trigger.add("found");
            oscP5.send(trigger, myRemoteLocation); 
            wrongFile = false;}
         if(pathName.equals("ronny"))
          {
            textEngine.addText(loadStrings("ronny.txt"));
            OscMessage trigger = new OscMessage("/trigger");
            trigger.add("found");
            oscP5.send(trigger, myRemoteLocation);  
            wrongFile = false;} 
        }  
            
         if(pathName.equals("ekgpong")){
            //OscMessage myMessage = new OscMessage("pongstart");
            //oscP5.send(myMessage, myRemoteLocation);
            state = 2;
            wrongFile = false;}
        if(pathName.equals("thewall")){
            //OscMessage myMessage = new OscMessage("thewall");
            //oscP5.send(myMessage, myRemoteLocation);
            state = 3;
            wrongFile = false;}     
           
        } 
      }
    }
    if (wrongFile == true)// && fileError == false)
    {
      //fileError = true;
      textEngine.addString("<error 483: file does not exist or is not executable>");
    }
  }
  void changeDirectory(String pathName)
  {
    println("changedirectory1");
    int countFiles = curDir.getChildCount();
    boolean wrongFile = true;
    println("dir count " + countFiles);
    for(int i = 0; i < countFiles; i++)
    {
      println("YEEEEHAA " + countFiles);
      XMLElement folder = curDir.getChild(i);
      println("sandwich");
      String folderName = folder.getString("path").toLowerCase();
      println("path name is: " + pathName);
      println("folder name is: " + folderName);
      if(folderName.equals(pathName) == true)// bug? why not -> && file.getString("ext") != null);
      {
        if (folder.getString("ext") == null)
        {
          trackDepth[numDepth] = i;
          numDepth = numDepth + 1;
          println("int is " + i);
          String currentDir = xml.getChild(driveState).getString("path");
          if (folderName.equals("input_name"))
          {
            println("accesing input folder");
            inputName = true;
          } else { 
            println("current directory is: " + currentDir);
          }
          if (fullDirectory.equals(currentDir + "\\>"))
          {
            println("OOOOOOOOOOOOOOHAAAAA");
            String tempDirectory = fullDirectory.substring(0, fullDirectory.length()-1)  + folderName + ">";
            fullDirectory = tempDirectory;
            curDir = folder;
            println("[cd] directory is now " + curDir.getString("path"));
            wrongFile = false;
            break;
          } else
          {
            String tempDirectory = fullDirectory.substring(0, fullDirectory.length()-1) + "\\" + folderName + ">";
            fullDirectory = tempDirectory;
            curDir = folder;
            println("[cd] directory is now " + curDir.getString("path"));
            wrongFile = false;
            break;
          }
        } 
      }
    }  
    if (wrongFile == true)
    {
      textEngine.addString("error folder does not exist");
    }  
  }
} 
