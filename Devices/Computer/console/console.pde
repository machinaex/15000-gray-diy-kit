import oscP5.*;
import netP5.*;
import fullscreen.*;


OscP5 oscP5;
NetAddress myRemoteLocation;
FullScreen fs;
TextEngine textEngine; 
KeyInput keyInput;
Parser parser;
Execute execute;
Pong pong;
Board myBoard;
Board.BoardBlock[][] myBoardBlock;
fallingPiece myPiece;

//Tetris
int numRows = 15;
int numCol = 10;
int brickSize = 25;
int offset = 100;
int numRowsCleared;
boolean gameIsOver = false;


//Pong
int noteDuration = 100; // Duration of each note in milliseconds
float[] rawSequence = { 800, 550, 100 }; // right and left paddle impact, and scoreIncrease pitches

PFont Hira;
PFont scoreFont;
PFont playerFont;

final int fontSize = 15; 

int xDirection = 1;
int yDirection = 1;
//char keyp = 'S';
int circleDia = 10; // Diameter of the circle/square
float circleRad = circleDia/2; // Radius of the circle/square
float xBallPos, yBallPos, lPaddleHeight, rPaddleHeight;// Variables for the position of the ball and the paddles
int paddleWidth = 80;
int paddleHeight = 10;
int heart = 160;
float origSpeed = 10;
float speed = origSpeed; // Speed of the ball.  This increments as the game plays, to make it move faster
float speedInc = origSpeed/15;
float ySpeed = 0; // Start the ball completely flat.  Position of impact on the paddle will determine reflection angle.
int paused = 1; // Enable pausing of the game.  Start paused
float def = random (100); // Deflection of the Ball

int paddleJump = 10; // How fast the paddles move every frame

int multiPress = 0; // Initial setting of key states, ie 0 = nothing pressed
final static int LEFTUP = 1; // Use bitwise 'or' to track multiple key presses
final static int LEFTDOWN = 2; 
final static int RIGHTUP = 4; 
final static int RIGHTDOWN = 8; 

//Console

XMLElement xml;
XMLElement curDir;

int xmlNum;
int[] trackDepth;
int numDepth;
int state = 0;

String maxInput=""; 
String fullDirectory = "C:\\>";
boolean useScroll = true; 
boolean keyPress = false;
boolean exitTrue = false;
boolean yesNo = false;
boolean inputName = false;


static PApplet pa;

int maxLength = 65; 
int driveState = 0; //PUBLIC directory

void setup() 
{ 
  //link.declareInlet("s");
  oscP5 = new OscP5(this,12000);
  myRemoteLocation = new NetAddress("192.168.0.2",12001);

  fs = new FullScreen(this); // Create the fullscreen object
  fs.enter(); // enter fullscreen mod
  fs.setResolution(800,600);
  fs.setShortcutsEnabled(false);
  
  //Tetris
  
  Hira = loadFont("OCRAStd-24.vlw");
  myBoard = new Board();
  myBoardBlock = new Board.BoardBlock[10][15];
    
    for (int i=0; i<numCol; i++){
    for (int j=0; j<numRows; j++){
      myBoardBlock[i][j] = myBoard.new BoardBlock(i,j);
    }
  }
  
  myPiece = new fallingPiece((int)random(6.99999));
  
  //Pong
    
  background(0);
  smooth();
  noStroke();
  //stroke(120, 240, 40);
  scoreFont = loadFont("OCRAStd-10.vlw"); // Score font
  playerFont = loadFont("OCRAStd-10.vlw"); // Player font
  frameRate(60);
  xBallPos = width/2; // Start the ball in the centre
  yBallPos = height/2;
  lPaddleHeight = width+40;
  rPaddleHeight = width+80;
  
  size(800,600); 
  frameRate(30);
 
  pa = this;
  xml = new XMLElement(this, "DeceitTree.xml");
  curDir = xml.getChild(0);//is C:
 
  parser = new Parser(); 
  execute = new Execute();
  textEngine = new TextEngine(); 
  keyInput = new KeyInput();
  pong = new Pong();
}

void draw() 
{ 
  if (state == 0) { 
    noLoop();
    fill(0);
    rectMode(CORNER);
    rect(0, 0, width, height);
  } 
  else {
  if (state == 2) {    
  pong.drawYourself();
  }else
  if (state == 3) { 
  background(0);
  myBoard.renderBoard();
  myBoard.clearFullRows();
  myBoard.displayScore();
  myBoard.displayGameOver();
  myPiece.animate();
  } 
  else {
  if(state == 1) 
  {  
  loop();
  if(useScroll) 
  { 
    //Scroll the commands 
    textEngine.scrollText(); 
  } 
  else 
  { 
    //Just draw the commands 
    textEngine.draw(); 
  } 
 } 
 }  
} 
} 

void keyPressed() 
{ if(state == 0) { 
  keyPress = true; 
  char k = (char)key; 
  keyInput.update(k); 
  
  if (key == 'A') {
    key = 27;  // Fools! don't let them escape!
  }
  if (key == ESC) {
    key = 0;}
}
  if(state == 1) { 
  keyPress = true; 
  char k = (char)key; 
  keyInput.update(k); 
  
  if (key == 'A') {
    key = 27;  // Fools! don't let them escape!
  }
  if (key == ESC) {
    key = 0;}
}
  if(state == 2) { 
  switch(key) { 
    case('q'):case('Q'):multiPress |=LEFTUP;break; 
    case('w'):case('W'):multiPress |=LEFTDOWN;break; 
    case('o'):case('O'):multiPress |=RIGHTUP;break; 
    case('p'):case('P'):multiPress |=RIGHTDOWN;break; 
  }
  if (key == 'A') {
    key = 27;  // Fools! don't let them escape!
  }
  if (key == ESC) {
    key = 0;}
}
}

void keyReleased() {   
  switch(key) { 
    case('q'):case('Q'):multiPress ^=LEFTUP;break; 
    case('w'):case('W'):multiPress ^=LEFTDOWN;break; 
    case('o'):case('O'):multiPress ^=RIGHTUP;break; 
    case('p'):case('P'):multiPress ^=RIGHTDOWN;break; 
  }
}
  


void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("start")==true) {
    state = 1;
    textFont(loadFont("OCRAStd-10.vlw"),fontSize); 
    heart = 160;
    fs.setShortcutsEnabled(false);
    textEngine.addText(loadStrings("logomci.txt"));
    redraw();
    print("### received an osc message.");
    print(" addrpattern: "+theOscMessage.addrPattern());
    println(" typetag: "+theOscMessage.typetag());/* print the address pattern and the typetag of the received OscMessage */
  } 
    else if(theOscMessage.checkAddrPattern("pongboot")==true) {
    state = 1;
    heart = 160;
    textFont(loadFont("OCRAStd-10.vlw"),fontSize); 
    textEngine.addText(loadStrings("rescue.txt"));
    redraw();
   }
    else if(theOscMessage.checkAddrPattern("tetrisboot")==true) {
    state = 1;
    textFont(loadFont("OCRAStd-10.vlw"),fontSize); 
    textEngine.addText(loadStrings("firewall.txt"));
    redraw();    
  }
    else if(theOscMessage.checkAddrPattern("tetrissuck")==true) {
    state = 1;
    textFont(loadFont("OCRAStd-10.vlw"),fontSize); 
    textEngine.addText(loadStrings("tetrissuck.txt"));
    redraw();    
  }
    else if(theOscMessage.checkAddrPattern("nuke")==true) {
    state = 1;
    textEngine.addText(loadStrings("atombombe.txt"));
    redraw();    
  }
    else if(theOscMessage.checkAddrPattern("paula")==true) {
    state = 1;
    textEngine.addText(loadStrings("achive.txt"));
    redraw();    
  }
    else if (theOscMessage.checkAddrPattern("stop")==true){
    state = 0;
    redraw();
  }
    else if (theOscMessage.checkAddrPattern("quit")==true){
    state = 0;
    fs.setShortcutsEnabled(true);
    fs.leave();
    redraw();
  }
    else if
    (theOscMessage.checkAddrPattern("suck")==true) {
    textEngine.addText(loadStrings("skullgameover.txt"));
    redraw();} 
  
  if(theOscMessage.checkAddrPattern("text")==true) {
    maxInput = theOscMessage.get(0).stringValue();
  }
}
 


  

  


