/* this is the renderer for the real live adventure game "15000 Gray" from machina eX
 * code by Philip Steimel
 * concept developed by Robin Krause and Philip Steimel and machina eX
 *
 * licence: Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
 *
 * for commercial use please contact philip @ machinaex . de
 *
 * more information and hardware buildplan at:
 * https://bitbucket.org/machinaex/15000-gray-diy-kit/src
 * or
 * https://www.machinaex.de/15000-gray-diy-kit/
 *
 *
 * TO DO:
 *   - add games
 *   - change network API to JSON based strings instead of pure strings
*/

const fs = require("fs");
const path = require("path");
const howler = require('howler');

const electron = require('electron'); // in order to work within electron-framework

const net = require('net'); // for tcp
const dgram = require('dgram'); // for udp

var copy = new Howl({
      src: ['./sound/copy.mp3']
    });

let files = showDirSync("./root"); // files in the currentDir
let hiddenfiles = showDirSync("./hidden")
let root = __dirname+"/root"; // to set Boundaries to files reachable
let currentDir = root;

var udpOutPort = 8889;

console.log("root contains: "+ files);

$( document ).ready(function() {
  $( "#commandline" ).focus();
});

$("body").on( "click", function(){$( "#commandline" ).focus(); setCaret()});
$("body").on( "keydown", keyUpOrDown);
$("body").on( "keyup", keyUpOrDown);


function keyUpOrDown(event) {

  $( "#commandline" ).focus();

  if (event.key == "Enter" && event.type == "keydown")
  {
      event.preventDefault
      let text = $("#commandline").text();
      $("#commandline").html(""); // clear the line
      $("<div>"+text+"</div>").insertBefore($("#commandline"));

      filterCommands(text.substr(1).trim())
  }
  else
  {
    // else: keep this terminal intact:

    let text = $("#commandline").text();
    // here we can check for any changes within text.

    //check if first char empty and if so: add > at beginning:
    if (text.length < 2)
      {
        $("#commandline").html(">&nbsp;");
      }

    //set Cursor/Caret always to end of line.
    setCaret();

  }
}


///////////////////////////////////////////////////
/////// NETWORK MESSAGES ARRIVE HERE >>> //////////

electron.ipcRenderer.on('fromMain', (event, message) => {

  if (message != null)
  {

    console.log(message); // Prints whatever comes  in

    if (message.substr(0,4) == "type")
    {
      // write into commandline from external source:
      let totype = message.substr(5);
      $("#commandline").text($("#commandline").text()+totype)
    }
    else if (message.substr(0,5) == "black")
    {
      console.log("BLACK!!!!!!!!!!!!!!");
    }
    else if (message.substr(0,5) == "start")
    {
      console.log("START!!!!!!!!!!!!!!");
      writeOnScreen(openFile("./hidden/logomci.txt",true))

    }
    else if (message.substr(0,8) == "gameover")
    {
      console.log("GAMEOVER!!!!!!!!!!!");
      writeOnScreen(openFile("./hidden/skullgameover.txt",true))
    }
    else if (message.substr(0,8) == "atom")
    {
      console.log("ATOMBOMBE!!!!!!!!!!!");
      writeOnScreen(openFile("./hidden/atombombe.txt",true))
    }
    else if (message.substr(0,3) == "win")
    {
      console.log("WIN!!!!!!!!!!!!!!!!");
      writeOnScreen(openFile("./hidden/achieve.txt",true))
    }
    else if (message.substr(0,6) == "gameon")
    {
      console.log("lets play !!!!!!!!");
      $("body").prepend("<iframe id='gameoverlay' src='examplegame.html'>'");
    }
    else if (message.substr(0,7) == "gameoff")
    {
      console.log("lets play !!!!!!!!");
      $("#gameoverlay").remove();
    }

  }
});

///////////////////////////////////////////////////
/////// <<< NETWORK MESSAGES ARRIVE HERE  //////////

function filterCommands(cmd) {

  cmd = cmd.toLowerCase();
  let response = "";
  console.log(cmd);
  sendUDP("localhost",udpOutPort, cmd);

  if (cmd == "ls")
  {
      files.forEach(function(f){
          response = response + " "+f+" ";
      });
  }
  else if(cmd.trim().substr(1,2) == ":") // for A: B: C: D: etc.
  {
    console.log("trying to switch volume");
    currentDir = root+"/"+cmd.substr(2).trim();

    response = " moved to "+cmd;

  }
  else if (cmd == "help" || cmd == "--help")
  {
      response = openFile("../hidden/help.txt");
  }
  else if (cmd.substr(0,4) == "run " )
  {
    let filename = cmd.substr(4);
    response = openFile(filename);
  }
  else if (cmd.substr(0,2) == "./")
  {

      let filename = cmd.substr(2);
      response = openFile(filename);
  }
  else if (cmd.substr(0,2) == "cd")
  {
    if (cmd.trim() === "cd")
    {
      currentDir = root;
      files = showDirSync(root);
      response = " moved to root";
    }
    else
    {
        let mypath = currentDir+"/"+cmd.substr(2).trim();

        // to do: filter ../ that goes beyond root:
        mypath = path.join(mypath);
        //console.log(mypath.replace(/\//g,"").length);

        if (mypath.replace(/\//g,"").length < root.replace(/\//g,"").length)
        {
          mypath = currentDir;
          response = " NO ENTRY ALLOWED!!!"
        }
        else
        {

            try{
                files = showDirSync(mypath);
                response = " moved to: "+ mypath.replace(__dirname, "").replace("root/","");
                currentDir = mypath;
            }
            catch(error){
              response = " '"+ mypath.replace(root,"")+ "' is not a directory or does not exist.";
            }

        }
    }

  }
  else if (cmd.substr(0,4) == "copy")
  {
      let filename = cmd.substr(5).split(" ")[0];
      console.log(files);

      try
      {
        fs.readFileSync(currentDir+"/"+filename);// check if file exists...

        response = "<br>copying "+ filename +" to A:/" + filename + openFile("hidden/copy.txt",true);
        copy.play();
      }
      catch(e)
      {
        response = "<br> cannot find file to copy. Are you in the right directory?"
      }

  }
  else
  {
      response = " ERROR - UNKNOWN COMMAND";
  }

  writeOnScreen(response);

}

function writeOnScreen(inp)
{
  $("<div>"+inp+"</div>").insertBefore($("#commandline"));

  $(window).scrollTop($('#commandline').position().top);

}


function setCaret() { // set the cursor always at the end of the commandline
    var el = document.getElementById("commandline");
    var range = document.createRange();
    var sel = window.getSelection();
    var len = el.childNodes[el.childNodes.length-1].length;
    //console.log(el.childNodes.length);
    //console.log(el.childNodes);
    //console.log(el.childNodes.length);
    range.setStart(el.childNodes[el.childNodes.length-1], len);
    range.collapse(true);
    sel.removeAllRanges();
    sel.addRange(range);
    el.focus();
}


// List all files in dir
function showDirSync (dir, filelist) {
            var path = path || require('path');
            var fs = fs || require('fs'),
                files = fs.readdirSync(dir);
            filelist = filelist || [];
            files.forEach(function(file) {
                    //filelist.push(path.join(dir, file));

                    if (fs.lstatSync(path.join(dir,file)).isDirectory())
                    {
                      filelist.push(file+"/");
                    }
                    else
                    {
                      filelist.push(file);
                    }

                });
            return filelist;
};


//////////////////////////////////////////////////////////
/////////// HERE HAPPENS ALL THE MAGIC //////////////////

function openFile(filename, absPath)
{
  console.log(filename);
  console.log(currentDir+"/"+filename);
  // OPEN FILE!
  let output = ""
  try
  {

      // tbd: HERE WE SHOULD CHECK FOR SPECIFIC FILES TO BE OPENED IF WE WANT
      //    THE SYSTEM TO RECOGNIZE/REACT TO THESE EVENTS!!!!!
      let content;
      if (absPath)
      {
        content = fs.readFileSync(filename);
      }
      else
      {
        content = fs.readFileSync(currentDir+"/"+filename);
      }
      // transform the buffer into a string with nobreak spaces for ASCII Art to work:
      content.forEach(function(byte){
          if (byte == 32)
          {
              output = output+"\xa0";
          }
          else if (byte == 10 || byte == 13)
          {
            output = output+"<br>";
          }
          else
          {
              output = output+String.fromCharCode(byte);
          }
      })
  }
  catch(error)
  {
    output = "file does not exist in this place or is not executable."
  }
  console.log(output);
  return "<br>"+output+"<br><br>";
}




/////////// NETWORK OUT ///////////////////////

function sendTCP(url,port,message)
{
	var client = new net.Socket();
	client.connect(port, url, function() {

		console.log('TCP send "' + message + '" to: ' + url + ':' + port);
		// Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
		client.write(message);
		client.destroy(); // if no response is to be awaited else: comment this line out!
	});
}


function sendUDP(url,port,message)
{
	var message = new Buffer(message);

	var client = dgram.createSocket('udp4');
	client.send(message, 0, message.length, port, url, function(err, bytes) {
	    if (err) throw err;
	    //console.log('UDP message sent to ' + port +':'+ url);
	    client.close();
	});

}
