void kabelRaetsel()
{ 
 // prototypisch. am besten: änderung abfragen und status in String speichern.
 
  digitalWrite(OUT_1, LOW);
  inputReading(1);
  digitalWrite(OUT_1, HIGH);

  digitalWrite(OUT_2, LOW);
  inputReading(2);
  digitalWrite(OUT_2, HIGH);
  
  digitalWrite(OUT_3, LOW);
  inputReading(3);
  digitalWrite(OUT_3, HIGH);

  digitalWrite(OUT_4, LOW);
  inputReading(4);
  digitalWrite(OUT_4, HIGH);
  /*
  Serial.print(in_1_status);
  Serial.print(in_2_status);
  Serial.print(in_3_status);
  Serial.println(in_4_status);
  */
  kabelStatus = "";
  kabelStatus = String(in_1_status) + String(in_2_status) + String(in_3_status) + String(in_4_status);
 
  in_1_status = 0; in_2_status = 0; in_3_status = 0; in_4_status = 0; 
  
  
  if(kabelStatus != kabelStatusAlt)
  {
    //Serial.println(kabelStatus);
    if (pushCables == true && (now - lastupdateCables) > 500)
    {
      lastupdateCables = now;
      pushCableStati();
      kabelStatusAlt = kabelStatus;
    }
    
  }
  
  
}


void pushCableStati()
{
  char message[20] = "1234";
  
  String clockpauseStr("");
  clockpauseStr += kabelStatus;
  clockpauseStr.toCharArray(message, 20);
  msgReplyCables(message);
}


void inputReading(int num)
{
  
    if ( digitalRead(IN_1) == LOW )
    {
       in_1_status = num; 
    }
    if ( digitalRead(IN_2) == LOW )
    {
       in_2_status = num; 
    }
    if ( digitalRead(IN_3) == LOW )
    {
       in_3_status = num; 
    }
    if ( digitalRead(IN_4) == LOW )
    {
       in_4_status = num; 
    }
  
}
