{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 8,
			"architecture" : "x64"
		}
,
		"rect" : [ 227.0, 135.0, 1142.0, 960.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 44.5, 343.669434, 112.0, 20.0 ],
					"text" : "rbtnk.autoBpatcher",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 318.25, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 279.25, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 240.25, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 198.0, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 159.0, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 120.0, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 81.0, 55.0, 32.5, 20.0 ],
					"text" : "+ 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-13",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 42.0, 3.0, 25.0, 25.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 315.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 273.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 276.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 234.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 237.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 195.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 198.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 156.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 159.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 117.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 120.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 78.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 81.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "bpatcher",
					"name" : "dmx_slider_new.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 42.0, 126.0, 39.0, 160.50415 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 39.0, 160.50415 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.222174, 0.220101, 0.181778, 1.0 ],
					"bordercolor" : [ 0.222174, 0.220101, 0.181778, 1.0 ],
					"id" : "obj-21",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 305.0, 437.0, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 312.0, 160.50415 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "dmx_slider_new.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/MODULE",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.js",
				"bootpath" : "/Users/Limor/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "TEXT",
				"implicit" : 1
			}
 ]
	}

}
