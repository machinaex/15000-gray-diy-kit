{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 8,
			"architecture" : "x86"
		}
,
		"rect" : [ 142.0, 44.0, 1313.0, 834.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-92",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 267.243134, 357.0, 35.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 317.79541, 354.038513, 35.0, 18.0 ],
					"text" : "suck"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 443.79541, 366.445435, 90.0, 20.0 ],
					"text" : "s computerudp"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-82",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 346.79541, 357.0, 40.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 363.79541, 354.038513, 40.0, 18.0 ],
					"text" : "paula"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-5",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 437.5, 357.0, 37.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 269.79541, 354.038513, 37.0, 18.0 ],
					"text" : "nuke"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-80",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 132.79541, 357.0, 60.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 127.79541, 354.038513, 60.0, 18.0 ],
					"text" : "tetrisboot"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 192.79541, 357.0, 61.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 197.79541, 354.038513, 61.0, 18.0 ],
					"text" : "tetrissuck"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-71",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 84.79541, 357.0, 33.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 85.79541, 354.038513, 33.0, 18.0 ],
					"text" : "stop"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 50.79541, 357.0, 34.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 51.79541, 354.038513, 34.0, 18.0 ],
					"text" : "start"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 504.0, 730.0, 80.0, 18.0 ],
					"text" : "TELSOLVED"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 426.0, 802.0, 60.0, 20.0 ],
					"text" : "s EVENT"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 361.0, 687.0, 223.0, 20.0 ],
					"text" : "sel TELCUT TELSOLVED RESET TEL1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-89",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 453.0, 730.0, 32.5, 18.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-87",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 393.0, 730.0, 32.5, 18.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 426.0, 759.0, 34.0, 20.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 402.0, 639.0, 58.0, 20.0 ],
					"text" : "r EVENT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "bpatcher",
					"name" : "bombe_Yun_Modul.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 555.738037, 447.430328, 672.0, 100.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 529.475281, 443.430328, 672.0, 116.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1078.797119, 357.0, 112.0, 20.0 ],
					"text" : "rbtnk.autoBpatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "bpatcher",
					"name" : "TelUhrNum(Getrennt)_test.maxpat",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "bang", "bang", "bang" ],
					"patching_rect" : [ 8.0, 386.445435, 542.0, 229.969788 ],
					"presentation" : 1,
					"presentation_rect" : [ 10.475296, 386.966248, 519.0, 227.699997 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "bpatcher",
					"name" : "bombeXP_02.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 633.797119, 10.0, 580.0, 431.994934 ],
					"presentation" : 1,
					"presentation_rect" : [ 632.746155, 6.699219, 568.729126, 430.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "bpatcher",
					"name" : "device_object.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 8.0, 10.0, 615.486267, 338.278442 ],
					"presentation" : 1,
					"presentation_rect" : [ 10.475296, 6.699219, 615.486267, 338.278442 ],
					"varname" : "device_object"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.210312, 0.210312, 0.210312, 1.0 ],
					"border" : 5,
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.5 ],
					"id" : "obj-6",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 555.738037, 454.038513, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.475296, 0.699219, 1288.098633, 622.0 ],
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-44::obj-135" : [ "textedit[10]", "textedit", 0 ],
			"obj-44::obj-132" : [ "textedit[9]", "textedit", 0 ],
			"obj-44::obj-50" : [ "textedit[7]", "textedit", 0 ],
			"obj-44::obj-87" : [ "textedit[8]", "textedit", 0 ],
			"obj-44::obj-43" : [ "textedit[6]", "textedit", 0 ],
			"obj-44::obj-34" : [ "textedit[5]", "textedit", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "device_object.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "COMPUTER.txt",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.js",
				"bootpath" : "/Users/Limor/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bombeXP_02.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "MachinaMessenger.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "TelUhrNum(Getrennt)_test.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bombe_Yun_Modul.maxpat",
				"bootpath" : "/Users/Limor/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "OpenSoundControl.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
