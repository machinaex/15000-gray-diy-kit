Control
-------
*(work in progress, noch nicht fertig)*

Was du hier findest ist für die zentrale technische Verwaltung von 15000 Gray zuständig.
Im Zentrum steht ein [Maxpatch](https://cycling74.com/products/max), in dem die technischen Cues ausgelöst werden, die dann alle nötigen Informationen an die angeschlossene Software und die Devices weitergibt.
Sounds, Musik und Hörspiele werden dann vom Liveset verwaltet.
Das ganze konnten wir seit unserem letzten 15000 Gray Gastspiel im Frühling 2016 leider nicht mehr testen und die Installationsanleitung ist auch noch nicht fertig, aber wenn du trotzdem schonmal loslegen willst findest du hier ein Tutorial von Mathias wie man Max und [Ableton Live](https://www.ableton.com/de/live/) miteinander flirten lässt:
[Immer dem Licht hinterher](https://www.youtube.com/watch?v=0SxpZtVra-k)


### TO BE DONE
Für die Zukunft planen wir alles was [Max/MSP](https://cycling74.com/products/max) zur Zeit erledigt, mit selbstgeschriebener, weniger proprietärer, plattformunabhängiger Software zu ersetzen. Weil Max/MSP und Ableton Live Versionen ohne Speicher-Funktion frei zum Download stehen und definitiv hervorragende Tools sind für das was wir tun, werden wir immer versuchen eine MAX und LIVE Version auf dem aktuellen Stand mitzuliefern.

### Netzwerk Kommunikation zwischen Control und den Devices:

(alle IP Adressen können natürlich im Code angepasst werden)
![](../Devices/img/network_map.png "")


- alle Geräte reden via WiFi mit dem Zentralcomputer.
- sie sprechen dabei UDP und verschicken Strings an Port 8888 am Zentralcomputer.
- der Zentralcomputer sendet ebenfalls UDP Strings an Port 8888 an die Devices um ihnen einzelne Befehle zu geben.

that's it!